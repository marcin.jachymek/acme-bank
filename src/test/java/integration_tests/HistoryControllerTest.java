package integration_tests;

import com.wujq.controller.HistoryController;
import com.wujq.domain.Transactions;
import configuration.AcmeTestContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AcmeTestContext.class}, loader = AnnotationConfigContextLoader.class)
public class HistoryControllerTest extends BaseTest {

    @Autowired
    HistoryController historyController;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(historyController).build();
    }

    @Test
    public void getTransactionHistoryTest() throws Exception {
        MvcResult result = mockMvc.perform(get(HISTORY_MAPPING + USER_1_ID))
                .andExpect(status().isOk())
                .andReturn();
        //check response format
        Transactions transactions = gson.fromJson(result.getResponse().getContentAsString(), Transactions.class);
    }

}
