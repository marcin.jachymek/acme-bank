package integration_tests;

import com.wujq.controller.BalanceController;
import com.wujq.controller.TokenController;
import com.wujq.domain.Balance;
import com.wujq.domain.Decrease;
import com.wujq.domain.Increase;
import com.wujq.domain.Token;
import configuration.AcmeTestContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AcmeTestContext.class}, loader = AnnotationConfigContextLoader.class)
public class BalanceControllerTest extends BaseTest {

    @Autowired
    BalanceController balanceController;

    @Autowired
    TokenController tokenController;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(balanceController).build();
    }


    @Test
    public void getBalanceTest() throws Exception {
        MvcResult result = mockMvc.perform(get(BALANCE_MAPPING + USER_1_ID))
                .andExpect(status().isOk())
                .andReturn();
        //check response format
        Balance balance = gson.fromJson(result.getResponse().getContentAsString(), Balance.class);
    }

    @Test
    public void increaseBalanceTest() throws Exception {
        Increase increase = new Increase(1);
        mockMvc.perform(post(BALANCE_MAPPING + USER_1_ID + INCREASE).content(gson.toJson(increase)).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void successfulDecreaseBalanceTest() throws Exception {
        Decrease decrease = new Decrease();
        MockMvc tokenControllerMock = MockMvcBuilders.standaloneSetup(tokenController).build();
        MvcResult result = tokenControllerMock.perform(get(TOKEN_MAPPING + USER_1_ID))
                .andExpect(status().isCreated())
                .andReturn();
        Token token = gson.fromJson(result.getResponse().getContentAsString(), Token.class);
        decrease.setValue(2);
        decrease.setToken(token.getToken());
        mockMvc.perform(post(BALANCE_MAPPING + USER_1_ID + DECREASE).content(gson.toJson(decrease)).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andReturn();
    }

    @Test
    public void failedDecreaseBalanceTest() throws Exception {
        Decrease decrease = new Decrease();
        decrease.setValue(2);
        decrease.setToken(UUID.randomUUID().toString());
        mockMvc.perform(post(BALANCE_MAPPING + USER_1_ID + DECREASE).content(gson.toJson(decrease)).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isForbidden())
                .andReturn();
    }

}