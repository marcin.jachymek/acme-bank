package integration_tests;

import com.wujq.controller.TokenController;
import com.wujq.domain.Token;
import configuration.AcmeTestContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AcmeTestContext.class}, loader = AnnotationConfigContextLoader.class)
public class TokenControllerTest extends BaseTest {

    @Autowired
    TokenController tokenController;

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(tokenController).build();
    }


    @Test
    public void getToken() throws Exception {
        MvcResult result = mockMvc.perform(get(TOKEN_MAPPING + USER_1_ID))
                .andExpect(status().isCreated())
                .andReturn();
        //response validation
        Token token = gson.fromJson(result.getResponse().getContentAsString(), Token.class);
    }
}
