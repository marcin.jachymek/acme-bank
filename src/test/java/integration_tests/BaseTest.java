package integration_tests;

import com.google.gson.Gson;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.Charset;

public class BaseTest {

    public static MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
    public static String BALANCE_MAPPING = "/balance/user/";
    public static String HISTORY_MAPPING = "/history/user/";
    public static String TOKEN_MAPPING = "/token/user/";
    public static String INCREASE = "/increase/";
    public static String DECREASE = "/decrease/";
    public static Integer USER_1_ID = 1;
    public static Integer USER_2_ID = 2;

    protected Gson gson = new Gson();
    protected MockMvc mockMvc;

}
