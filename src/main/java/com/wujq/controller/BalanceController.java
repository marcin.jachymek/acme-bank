package com.wujq.controller;

import com.wujq.domain.Decrease;
import com.wujq.domain.Increase;
import com.wujq.exception.IncorrectTokenException;
import com.wujq.service.BankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/balance/user/")
public class BalanceController {

    @Autowired
    BankService bankService;

    @GetMapping("{userId}")
    public ResponseEntity getCurrentBalance(@PathVariable("userId") Integer userId) {
        return new ResponseEntity(bankService.getBalanceForUser(userId), HttpStatus.OK);
    }

    @PostMapping("/{userId}/increase/")
    public ResponseEntity increaseBalance(@PathVariable("userId") Integer userId, @RequestBody Increase increase) {
        bankService.increaseBalance(userId, increase);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/{userId}/decrease/")
    public ResponseEntity decreaseBalance(@PathVariable("userId") Integer userId, @RequestBody Decrease decrease) {
        try {
            bankService.decreaseBalance(userId, decrease);
            return new ResponseEntity(HttpStatus.OK);
        } catch (IncorrectTokenException e) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

}
