package com.wujq.controller;

import com.wujq.service.BankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/history/user/")
public class HistoryController {

    @Autowired
    BankService bankService;

    @GetMapping("{userId}")
    public ResponseEntity getTransactionsHistory(@PathVariable("userId") Integer userId) {
        return new ResponseEntity(bankService.getTransactionHistory(userId), HttpStatus.OK);
    }


}
