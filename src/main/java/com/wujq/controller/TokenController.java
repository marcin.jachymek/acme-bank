package com.wujq.controller;

import com.wujq.service.BankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/token/user/")
public class TokenController {

    @Autowired
    BankService bankService;

    @GetMapping("{userId}")
    public ResponseEntity getToken(@PathVariable("userId") Integer userId) {
        return new ResponseEntity(bankService.generateTransactionTokenForUser(userId), HttpStatus.CREATED);
    }


}
