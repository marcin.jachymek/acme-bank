package com.wujq.service;

import com.wujq.domain.*;
import com.wujq.exception.IncorrectTokenException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class BankService {

    private Map<Integer, User> users = new HashMap<>();

    {
        User user1 = new User();
        User user2 = new User();
        user1.setUserId(1);
        user1.setBalance(new Balance(1));
        user1.generateTrancations();
        user2.setUserId(2);
        user2.setBalance(new Balance(2));
        user2.generateTrancations();
        users.put(user1.getUserId(), user1);
        users.put(user2.getUserId(), user2);
    }

    public Token generateTransactionTokenForUser(int userId) {
        Token token = new Token();
        token.setToken(users.get(userId).generateTransactionToken().toString());
        return token;
    }

    public Transactions getTransactionHistory(int userId) {
        return users.get(userId).getTransactions();
    }

    public void increaseBalance(int userId, Increase increase) {
        User user = users.get(userId);
        user.getTransactions().getTransactionsList().add(new Transaction("increase", increase.getValue()));
        user.changeBalance(increase.getValue());
    }

    public void decreaseBalance(int userId, Decrease decrease) throws IncorrectTokenException {
        User user = users.get(userId);
        if (checkIfTokenMatch(decrease, user)) {
            user.getTransactions().getTransactionsList().add(new Transaction("decrease", decrease.getValue()));
            user.changeBalance(-decrease.getValue());
        } else {
            throw new IncorrectTokenException();
        }
    }

    private boolean checkIfTokenMatch(Decrease decrease, User user) {
        return user.getTransactionToken().toString().equals(decrease.getToken());
    }

    public Balance getBalanceForUser(Integer userId) {
        return users.get(userId).getBalance();
    }
}
