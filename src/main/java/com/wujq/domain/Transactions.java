package com.wujq.domain;

import java.util.ArrayList;
import java.util.List;

public class Transactions {
    List<Transaction> transactions = new ArrayList<>();

    public List<Transaction> getTransactionsList() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }
}
