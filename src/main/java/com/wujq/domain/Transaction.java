package com.wujq.domain;

public class Transaction {
    String type;
    int value;

    public Transaction() {
    }

    public Transaction(String type, int value) {
        this.type = type;
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
