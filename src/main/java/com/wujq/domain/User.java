package com.wujq.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class User {
    int userId;
    Balance balance = new Balance();
    Transactions transactions = new Transactions();
    UUID transactionToken = UUID.randomUUID();

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Balance getBalance() {
        return balance;
    }

    public void setBalance(Balance balance) {
        this.balance = balance;
    }

    public Transactions getTransactions() {
        return transactions;
    }

    public void generateTrancations() {
        Random random = new Random();
        List<String> transactionType = new ArrayList<>();
        transactionType.add("increase");
        transactionType.add("decrease");
        for (int i = 0; i < 5; i++) {
            Transaction transaction = new Transaction();
            transaction.setType(transactionType.get(i % 2));
            transaction.setValue(random.nextInt(1000));
            transactions.getTransactionsList().add(transaction);
        }
    }

    public UUID getTransactionToken() {
        return transactionToken;
    }

    public UUID generateTransactionToken() {
        this.transactionToken = UUID.randomUUID();
        return this.transactionToken;
    }

    public void changeBalance(int change) {
        this.balance.value += change;
    }
}
