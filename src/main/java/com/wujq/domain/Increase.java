package com.wujq.domain;

public class Increase {
    Integer value;

    public Increase() {
    }

    public Increase(Integer value) {
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

}
